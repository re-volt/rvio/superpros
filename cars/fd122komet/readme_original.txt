================================================================
======================== Car Info ==============================
================================================================

Name				Flatboy
Author				Flyboy
Date				March 1, 2019
Type				Original
Folder Name			flatboy
Rating				Pro
Class				Glow
Drivetrain			Four-wheel Drive
Top speed			41 mph
Body mass			1 kg

================================================================
======================= Description ============================
================================================================

A Group C style car that has great acceleration and top speed,
but keep in mind it's prone to snap oversteer.


================================================================
======================== Credits ===============================
================================================================

Thanks to Kipy for helping me out with the Hul!



Tools:
Gimp
Blender and Marv's plugin
Prm2Hul (By Jig)
Autoshade

================================================================
======================== Changelog =============================
================================================================


================================================================
=================== Permissions and terms ======================
================================================================

(This creation don't have a license. Even so, please be polite and respect the authors wishes)

You're free to share (copy and redistribute the material in any medium or format) and adapt (remix, transform, and build upon the material) this creation, provided that:

1. You give appropriate credit and indicate if changes were made. You may do so in any reasonable manner.

2. You'll not use the material for commercial purposes.

3. If you remix, transform, or build upon the material, you must distribute your contributions under the same terms/permissions as the original.

4. You may not apply legal terms or technological measures that legally restrict others from doing anything the author permits.