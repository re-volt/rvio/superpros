Car information
================================================================
Car name : Mudman
Car Type : Repaint/Remap/Remodel
Top speed : 42 mph / 67 kph
Rating/Class : 4 (pro)
Installed folder : ...\cars\mudman
Description : Remap/Rehull/Repaint/etc of Freak Van (RVA original cars p.12, freak.zip on XTG). Models cleaned/fixed up a lot, sizes scaled down a bit, params mostly the same.

Blasting acceleration, 42mph top speed and SNW-tier grip would make this van freakishly overpowered in most cases. However the grip is also the main problem when combined with their large size and the soft suspension.
Rides up over obstacles and cars instead of bumping into them, easily spins out if traction gets unbalanced the wrong way.

Depending on the track it can either dominate or be lap traffic fodder.


Author Information
================================================================
Author Name : Raster Gotolei
Email Address : rgotolei@gmail.com

Construction
================================================================
Base : Freak Van by Megalon
Editor(s) used : blender for remapping, gimp for skin/carbox, inkscape for some details, gedit for text stuff

Notes
================================================================
wheel-flat-x.prm are flat-faced wheels

Additional Credits
================================================================
Megalon for original car
Jigebren for Blender plugin

Copyright / Permissions
================================================================
You may do whatever you want with this car.
XCFs/SVGs and a bunch of other junk in RVL thread download.
