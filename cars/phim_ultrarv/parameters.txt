{

;============================================================
;============================================================
; Ultra RV
;============================================================
;============================================================
Name       	"Ultra RV"

;====================
; Models
;====================

MODEL 	0 	"cars/phim_ultraRV/body.prm"
MODEL 	1 	"cars/phim_ultraRV/wheel-l.prm"
MODEL 	2 	"cars/phim_ultraRV/wheel-r.prm"
MODEL 	3 	"NONE"
MODEL 	4 	"NONE"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars/misc/Aerial.m"
MODEL 	18 	"cars/misc/AerialT.m"
TPAGE 		"cars/phim_ultraRV/car.bmp"
;)TCARBOX  	"cars/phim_ultraRV/box.bmp"
;)TSHADOW 	"cars/phim_ultraRV/shadow.bmp"
;)SHADOWTABLE 	-75.2905 75.2905 75.1757 -75.1757 -1.5660
COLL 		"cars/phim_ultraRV/hull.hul"
EnvRGB 		200 200 200

;====================
; Frontend
;====================

BestTime   	TRUE
Selectable 	TRUE
;)CPUSelectable	TRUE
;)Statistics 	TRUE
Class      	0
Obtain     	0
Rating     	5
TopEnd     	4119.989746
Acc        	6.304315
Weight     	1.850000
Trans      	2
MaxRevs    	0.500000

;====================
; Handling
;====================

SteerRate  	3.000000
SteerMod   	0.000000
EngineRate 	5.000000
TopSpeed   	44.000000
DownForceMod	2.000000
CoM        	0.000000 -3.750000 5.250000
Weapon     	0.000000 -32.000000 64.000000

;====================
; Body
;====================

BODY {		; Start Body
ModelNum   	0
Offset     	0.000000 0.000000 0.000000
Mass       	1.850000
Inertia    	1200.000000 0.000000 0.000000
           	0.000000 2150.000000 0.000000
           	0.000000 0.000000 800.000000
Gravity    	2200
Hardness   	0.000000
Resistance 	0.001000
AngRes     	0.001000
ResMod     	25.000000
Grip       	0.010000
StaticFriction 	0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Wheels
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-21.500000 0.000000 30.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	TRUE
SteerRatio  	-0.300000
EngineRatio 	0.000000
Radius      	10.500000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	6.000000
SkidWidth   	13.000000
ToeIn       	0.000000
AxleFriction    0.020000
Grip            0.024000
StaticFriction  2.400000
KineticFriction 2.330000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	21.500000 0.000000 30.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	TRUE
SteerRatio  	-0.300000
EngineRatio 	0.000000
Radius      	10.50000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	6.000000
SkidWidth   	13.000000
ToeIn       	0.000000
AxleFriction    0.020000
Grip            0.024000
StaticFriction  2.400000
KineticFriction 2.330000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-21.500000 0.000000 -40.750000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.000000
EngineRatio 	36500.000000
Radius      	10.50000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	6.000000
SkidWidth   	13.000000
ToeIn       	0.000000
AxleFriction    0.050000
Grip            0.025000
StaticFriction  2.470000
KineticFriction 2.400000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	2
Offset1  	21.500000 0.000000 -40.750000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.000000
EngineRatio 	36500.000000
Radius      	10.50000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	6.000000
SkidWidth   	13.000000
ToeIn       	0.000000
AxleFriction    0.050000
Grip            0.025000
StaticFriction  2.470000
KineticFriction 2.400000
}          	; End Wheel

;====================
; Springs
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	700.000000
Damping     	10.000000
Restitution 	-0.900000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	700.000000
Damping     	10.000000
Restitution 	-0.900000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	700.000000
Damping     	10.000000
Restitution 	-0.900000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	700.000000
Damping     	10.000000
Restitution 	-0.900000
}           	; End Spring

;====================
; Pins
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

;====================
; Axles
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

;====================
; Spinner
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
}           	; End Spinner

;====================
; Aerial
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	20.000000 -19.000000 -44.000000
Direction   	0.000000 -1.000000 0.000000
Length      	17.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial

;====================
; AI
;====================

AI {        	; Start AI
UnderThresh 	196.190002
UnderRange  	1660.855469
UnderFront  	2810.185791
UnderRear   	734.534729
UnderMax    	0.950000
OverThresh  	1247.171387
OverRange   	2196.582031
OverMax     	0.790000
OverAccThresh  	368.045593
OverAccRange   	494.211060
PickupBias     	16383
BlockBias      	16383
OvertakeBias   	16383
Suspension     	9830
Aggression     	0
}           	; End AI